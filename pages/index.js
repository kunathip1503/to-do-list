import React, { useState, useEffect } from "react";
import {
  Box,
  Typography,
  TextField,
  IconButton,
  List,
  ListItem,
  ListItemText,
  Checkbox,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import NoteAddIcon from "@mui/icons-material/NoteAdd";

export default function Home() {
  const [tasks, setTasks] = useState([]);
  const [newTask, setNewTask] = useState("");
  const [editingIndex, setEditingIndex] = useState(null);

  useEffect(() => {
    const storedTasks = localStorage.getItem("tasks");
    if (storedTasks) {
      setTasks(JSON.parse(storedTasks));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("tasks", JSON.stringify(tasks));
  }, [tasks]);

  const handleInputChange = (e) => {
    setNewTask(e.target.value);
  };

  const handleAddTask = (e) => {
    if (e.key === "Enter" && newTask.trim()) {
      setTasks([...tasks, { text: newTask.trim(), completed: false }]);
      setNewTask("");
    }
  };

  const handleDeleteTask = (index) => {
    const updatedTasks = [...tasks];
    updatedTasks.splice(index, 1);
    setTasks(updatedTasks);
    setEditingIndex(null);
  };

  const handleEditTask = (index) => {
    setEditingIndex(index);
  };

  const handleUpdateTask = (e, index) => {
    if (e.key === "Enter" || e.type === "blur") {
      const updatedTasks = [...tasks];
      updatedTasks[index] = {
        ...updatedTasks[index],
        text: e.target.value.trim(),
      };
      setTasks(updatedTasks);
      setEditingIndex(null);
    }
  };

  const handleToggleTaskCompletion = (index) => {
    const updatedTasks = [...tasks];
    updatedTasks[index] = {
      ...updatedTasks[index],
      completed: !updatedTasks[index].completed,
    };
    setTasks(updatedTasks);
  };

  return (
    <Box
      display="flex"
      flexDirection="column"
      alignItems="center"
      height="100vh"
      bgcolor="#121212"
      color="#fff"
      margin={8}
    >
      <Box sx={{ marginBottom: "2rem" }}>
        <Typography variant="h4" gutterBottom>
          What's the plan for today?
        </Typography>
      </Box>
      <Box sx={{ marginBottom: "2rem" }}>
        <Box display="flex" alignItems="center" mb={2}>
          <TextField
            value={newTask}
            onChange={handleInputChange}
            onKeyDown={handleAddTask}
            variant="outlined"
            placeholder="Add new task..."
            InputProps={{
              style: {
                borderRadius: "1rem",
                backgroundColor: "#333",
                color: "#fff",
                width: "765px",
                padding: "8px 16px",
              },
            }}
            sx={{
              flex: 1,
              marginRight: 8,
              "& .MuiOutlinedInput-root": {
                "&:hover fieldset": {
                  borderColor: "#fff",
                },
                "&.Mui-focused fieldset": {
                  borderColor: "#fff",
                },
              },
            }}
          />
        </Box>
        <List
          style={{
            width: "700px",
            height: "auto",
            backgroundColor: "#333",
            borderRadius: "1rem",
            padding: "2rem",
          }}
        >
          {tasks.map((task, index) => (
            <ListItem
              key={index}
              dense
              disableGutters
              style={{ marginBottom: 8 }}
              secondaryAction={
                editingIndex === index ? (
                  <>
                    <IconButton
                      edge="end"
                      aria-label="save"
                      onClick={() => setEditingIndex(null)}
                    >
                      <AddIcon sx={{ color: "white" }} />
                    </IconButton>
                    <IconButton
                      edge="end"
                      aria-label="delete"
                      onClick={() => handleDeleteTask(index)}
                    >
                      <DeleteIcon sx={{ color: "white" }} />
                    </IconButton>
                  </>
                ) : (
                  <>
                    <IconButton
                      edge="end"
                      aria-label="edit"
                      onClick={() => handleEditTask(index)}
                    >
                      <EditIcon sx={{ color: "white" }} />
                    </IconButton>
                    <IconButton
                      edge="end"
                      aria-label="delete"
                      onClick={() => handleDeleteTask(index)}
                    >
                      <DeleteIcon sx={{ color: "white" }} />
                    </IconButton>
                  </>
                )
              }
            >
              <Checkbox
                checked={task.completed}
                onChange={() => handleToggleTaskCompletion(index)}
                sx={{
                  color: "white",
                  "&.Mui-checked": {
                    color: "gray",
                  },
                }}
              />
              {editingIndex === index ? (
                <TextField
                  autoFocus
                  defaultValue={task.text}
                  onKeyDown={(e) => handleUpdateTask(e, index)}
                  onBlur={(e) => handleUpdateTask(e, index)}
                  sx={{
                    flex: 1,
                    marginRight: 8,
                    "& .MuiOutlinedInput-root": {
                      "&:hover fieldset": {
                        borderColor: "#fff",
                      },
                      "&.Mui-focused fieldset": {
                        borderColor: "#fff",
                      },
                    },
                  }}
                />
              ) : (
                <ListItemText
                  primary={task.text}
                  style={{
                    color: task.completed ? "gray" : "#fff",
                    marginRight: 8,
                    textDecoration: task.completed ? "line-through" : "none",
                  }}
                />
              )}
            </ListItem>
          ))}
          {tasks.length === 0 && (
            <Box display="flex" flexDirection="column" alignItems="center">
              <NoteAddIcon sx={{ fontSize: "20rem", color: "white" }} />
              <Typography
                align="center"
                color="white"
                sx={{ fontSize: "2rem" }}
              >
                Add Task to get started...
              </Typography>
            </Box>
          )}
        </List>
      </Box>
    </Box>
  );
}
